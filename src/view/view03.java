package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class view03 extends JFrame{
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	private JPanel background;
	private JPanel panel;
	private JCheckBox red_check;
	private JCheckBox green_check;
	private JCheckBox blue_check;
	

	
	public view03(){
		createCheckButton();
		createPanel();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setVisible(true);
		setResizable(false);
	} 
	
	private void createCheckButton(){
		
		red_check = new JCheckBox("RED");
		green_check = new JCheckBox("GREEN");
		blue_check = new JCheckBox("BLUE");	
		red_check.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent event){
				if(red_check.isSelected()){
					background.setBackground(Color.RED);
					panel.setBackground(Color.RED);
				}
				if(red_check.isSelected() && green_check.isSelected()){
					background.setBackground(Color.YELLOW);
					panel.setBackground(Color.YELLOW);
				}
				if(red_check.isSelected() && green_check.isSelected() && blue_check.isSelected()){
					background.setBackground(Color.GRAY);
					panel.setBackground(Color.GRAY);
				}
				}
			});
		green_check.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent event){
				if(green_check.isSelected()){
					background.setBackground(Color.GREEN);
					panel.setBackground(Color.GREEN);
				}
				if(green_check.isSelected() && red_check.isSelected()){
					background.setBackground(Color.YELLOW);
					panel.setBackground(Color.YELLOW);
				}
				if(green_check.isSelected() && red_check.isSelected() && blue_check.isSelected()){
					background.setBackground(Color.GRAY);
					panel.setBackground(Color.GRAY);
				}
				}
			});
		blue_check.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent event){
				if(blue_check.isSelected()){
					background.setBackground(Color.BLUE);
					panel.setBackground(Color.BLUE);
				}
				if(blue_check.isSelected() && red_check.isSelected()){
					background.setBackground(Color.PINK);
					panel.setBackground(Color.PINK);
				}
				if(blue_check.isSelected() && red_check.isSelected() && green_check.isSelected()){
					background.setBackground(Color.GRAY);
					panel.setBackground(Color.GRAY);
				}
				}
			});
	}
	
	private void createPanel(){
		background = new JPanel();
		panel = new JPanel();
		background.setLayout(new BorderLayout());
		panel.add(red_check); 
		panel.add(green_check); 
		panel.add(blue_check); 
		background.add(panel,BorderLayout.SOUTH);
		add(background);
		
	 }
	
}
