package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class view01 extends JFrame{
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	private JPanel background;
	private JPanel panel;
	private JButton red_button;
	private JButton green_button;
	private JButton blue_button;
	

	
	public view01(){
		createButton();
		createPanel();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setVisible(true);
		setResizable(false);
	} 
	
	private void createButton(){
		
		red_button = new JButton("RED");
		green_button = new JButton("GREEN");
		blue_button = new JButton("BLUE");
		red_button.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.RED);
				panel.setBackground(Color.RED);
				}
			});
		green_button.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.GREEN);
				panel.setBackground(Color.GREEN);
				}
			});
		blue_button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				background.setBackground(Color.BLUE);
				panel.setBackground(Color.BLUE);
				}
			});
	}
	
	private void createPanel(){
		background = new JPanel();
		panel = new JPanel();
		background.setLayout(new BorderLayout());
		panel.add(red_button); 
		panel.add(green_button); 
		panel.add(blue_button); 
		background.add(panel,BorderLayout.SOUTH);
		add(background);
		
	 }
	
}
